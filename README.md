# go-logger-wrapper

A simple wrapper library for personal use in public and private projects of mine.
If any issues arrise, please open an issue here: https://gitlab.com/dolfen-software/go-logger-wrapper/-/issues
Thanks.
