package logger

import log "github.com/visionmedia/go-cli-log"

var LOGLEVEL = 4

func DefineLogLevel(logLevel int) {
	LOGLEVEL = logLevel
}

func Info(key string, format string, args ...interface{}) {
	if LOGLEVEL > 2 {
		log.Info(key, format, args...)
	}
}

func Debug(key string, format string, args ...interface{}) {
	if LOGLEVEL > 3 {
		log.Verbose = true
	}
	log.Debug(key, format, args...)
}

func Warn(msg string, args ...interface{}) {
	if LOGLEVEL > 1 {
		log.Warn(msg, args...)
	}
}

func Error(err error) {
	if LOGLEVEL > 0 {
		log.Error(err)
	}
}
